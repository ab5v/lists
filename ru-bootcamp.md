## Буткемпы

* https://elbrusboot.camp/
* https://javabootcamp.ru/

## Курсы

* https://htmlacademy.ru/
* https://praktikum.yandex.ru/
* https://learn.python.ru/
* https://loftschool.com/

## Агрегаторы

* https://www.switchup.org/
* https://www.coursereport.com/schools

## Статьи
* [В программисты за полгода: что такое буткемпы и как освоить кодинг в рекордные сроки](https://theoryandpractice.ru/posts/17581-v-programmisty-za-polgoda-chto-takoe-butkempy-i-kak-osvoit-koding-v-rekordnye-sroki)
* [Как найти курсы программирования и чего стоят гарантии трудоустройства](https://habr.com/ru/post/464393/)